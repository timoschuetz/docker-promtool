# This file is a template, and might need editing before it works on your project.
FROM centos:7

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN yum update -y && yum install curl -y && yum clean all
RUN curl -LO https://github.com/prometheus/prometheus/releases/download/v2.22.0/prometheus-2.22.0.linux-amd64.tar.gz
RUN tar -xvf prometheus-2.22.0.linux-amd64.tar.gz
RUN mv prometheus-2.22.0.linux-amd64 prometheus-files
